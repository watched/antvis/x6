import { ConnectionStrategy } from '.'

export const noop: ConnectionStrategy.Definition = (terminal) => terminal
